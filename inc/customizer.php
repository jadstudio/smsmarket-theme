<?php
add_action('customize_register', 'sms_custom_contact_option');
add_action('customize_register', 'sms_custom_social_option');
function sms_custom_contact_option($wp_customize)
{

    $wp_customize->add_section('sms_contact_data_section', array(
        'title' => __('Contact Info', 'smsmarket-theme'),
        'priority' => 20,
        'description' => __('Contact Data', 'smsmarket-theme'),
    ));

    $wp_customize->add_setting(
        'sms_phone_number',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'sms_phone_number',
        array(
            'label' => __('Phone Number', ''),
            'section' => 'sms_contact_data_section',
            'settings' => 'sms_phone_number',
            'priority' => 2,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'sms_fax_number',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'sms_fax_number',
        array(
            'label' => __('Fax Number', ''),
            'section' => 'sms_contact_data_section',
            'settings' => 'sms_fax_number',
            'priority' => 2,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'sms_address',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
//            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'sms_address',
        array(
            'label' => __('Address', ''),
            'section' => 'sms_contact_data_section',
            'settings' => 'sms_address',
            'priority' => 1,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'sms_email',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'sms_email',
        array(
            'label' => __('Email', ''),
            'section' => 'sms_contact_data_section',
            'settings' => 'sms_email',
            'priority' => 3,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'sms_copyright',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'sms_copyright',
        array(
            'label' => __('Copyright', ''),
            'section' => 'sms_contact_data_section',
            'settings' => 'sms_copyright',
            'priority' => 4,
            'type'        => 'text',
        )
    ));

}

function sms_custom_social_option($wp_customize)
{

    $wp_customize->add_section('sms_social_section', array(
        'title' => __('Social links', 'medic8-theme'),
        'priority' => 20,
        'description' => __('Social links', 'medic8-theme'),
    ));

    $wp_customize->add_setting(
        'sms_facebook',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'sms_facebook',
        array(
            'label' => __('Facebook', ''),
            'section' => 'sms_social_section',
            'settings' => 'sms_facebook',
            'priority' => 2,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'sms_instagram',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
//            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'sms_instagram',
        array(
            'label' => __('Instagram', ''),
            'section' => 'sms_social_section',
            'settings' => 'sms_instagram',
            'priority' => 1,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'sms_twitter',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'sms_twitter',
        array(
            'label' => __('Twitter', ''),
            'section' => 'sms_social_section',
            'settings' => 'sms_twitter',
            'priority' => 3,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'sms_linkedin',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'sms_linkedin',
        array(
            'label' => __('Linkedin', ''),
            'section' => 'sms_social_section',
            'settings' => 'sms_linkedin',
            'priority' => 4,
            'type'        => 'text',
        )
    ));

}

add_action( 'customize_preview_init', 'sms_customizer_script' );
function sms_customizer_script() {
    wp_enqueue_script( 'sms1-customizer-script', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ) );
}