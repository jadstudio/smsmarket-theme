<?php

function sms_theme_scripts()
{
    wp_enqueue_style('sms1-theme-style', get_stylesheet_uri());
    wp_enqueue_script('sms1-all-js', get_template_directory_uri() . '/js/all.js', array( 'jquery' ));
}
add_action('wp_enqueue_scripts', 'sms_theme_scripts');

register_nav_menus( array(
    'header_menu' => 'Header menu',
    'footer_menu' => 'Footer menu'
) );


add_action( 'after_setup_theme', 'sms_after_setup' );

function sms_after_setup(){
    add_theme_support('menus');
    add_theme_support('custom-logo');
    add_theme_support('post-thumbnails');
    add_theme_support('html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ));
}

require get_template_directory() . '/inc/customizer.php';

add_action( 'widgets_init', 'sms_widgets_init' );

function sms_widgets_init(){
    register_sidebar( array(
        'name'          => 'Footer Area 1',
        'id'            => 'footer_1',
        'before_widget' => '<div class="widget-area widget-area_first">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-area-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer Area 2',
        'id'            => 'footer_2',
        'before_widget' => '<div class="widget-area widget-area_second">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-area-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer Area 3',
        'id'            => 'footer_3',
        'before_widget' => '<div class="widget-area widget-area_third">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-area-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => 'Sidebar',
        'id'            => 'sidebar_widget',
        'before_widget' => '<div class="widget-area widget-area_sidebar">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-area-title">',
        'after_title'   => '</h4>',
    ) );

    register_sidebar( array(
        'name'          => 'Learn sidebar',
        'id'            => 'learn_sidebar_widget',
        'before_widget' => '<div class="widget-area widget-area_sidebar">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-area-title">',
        'after_title'   => '</h4>',
    ) );
}

function sms_theme_text_marketing_posts() {

    /**
     * Post Type: Text marketing
     */

    $labels = array(
        "name" => __( 'Text marketing', '' ),
        "singular_name" => __( 'Text marketing', '' ),
    );

    $args = array(
        "label" => __( 'Text marketing', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "taxonomies" => array("text_marketing_category"),
        "rewrite" => array( "slug" => "text_marketing_posts", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "text_marketing_posts", $args );
}

add_action( 'init', 'sms_theme_text_marketing_posts' );

function sms_text_marketing_taxonomy(){
    register_taxonomy('text_marketing_category', array('text_marketing_posts'), array(
        'label'                 => 'Text marketing Category', 
        'labels'                => array(
            'name'              => 'Text marketing categories',
            'singular_name'     => 'Text marketing category',
            'search_items'      => 'Search Category',
            'all_items'         => 'All Category',
            'parent_item'       => 'Parent Category',
            'parent_item_colon' => 'Parent Genre:',
            'edit_item'         => 'Edit Category',
            'update_item'       => 'Update Category',
            'add_new_item'      => 'Add New Category',
            'new_item_name'     => 'New Category Name',
            'menu_name'         => 'Text marketing category',
        ),
        'description'           => '',
        'public'                => true,
        'publicly_queryable'    => null,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => true,
        'show_in_rest'          => null,
        'rest_base'             => null,
        'hierarchical'          => true,
        'update_count_callback' => '',
        'rewrite'               => array('slug' => "text_marketing_category"),
        //'query_var'             => $taxonomy, 
        'capabilities'          => array(),
        'meta_box_cb'           => null,
        'show_admin_column'     => true,
        '_builtin'              => false,
        'show_in_quick_edit'    => null,
    ) );
}


add_action('init', 'sms_text_marketing_taxonomy');


function sms_theme_learn_posts() {

    /**
     * Post Type: Learn post type
     */

    $labels = array(
        "name" => __( 'Learn', '' ),
        "singular_name" => __( 'Learn', '' ),
    );

    $args = array(
        "label" => __( 'Learn', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "taxonomies" => array("learn_category"),
        "rewrite" => array( "slug" => "learn_posts", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "learn_posts", $args );
}

add_action( 'init', 'sms_theme_learn_posts' );

function sms_learn_taxonomy(){
    register_taxonomy('learn_category', array('learn_posts'), array(
        'label'                 => 'Learn Category',
        'labels'                => array(
            'name'              => 'Learn categories',
            'singular_name'     => 'Learn category',
            'search_items'      => 'Search Category',
            'all_items'         => 'All Category',
            'parent_item'       => 'Parent Category',
            'parent_item_colon' => 'Parent Genre:',
            'edit_item'         => 'Edit Category',
            'update_item'       => 'Update Category',
            'add_new_item'      => 'Add New Category',
            'new_item_name'     => 'New Category Name',
            'menu_name'         => 'Learn category',
        ),
        'description'           => '',
        'public'                => true,
        'publicly_queryable'    => null,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => true,
        'show_in_rest'          => null,
        'rest_base'             => null,
        'hierarchical'          => true,
        'update_count_callback' => '',
        'rewrite'               => array('slug' => "learn_category"),
        //'query_var'             => $taxonomy,
        'capabilities'          => array(),
        'meta_box_cb'           => null,
        'show_admin_column'     => true,
        '_builtin'              => false,
        'show_in_quick_edit'    => null,
    ) );
}


add_action('init', 'sms_learn_taxonomy');

function sms_industries_posts() {

    /**
     * Post Type: Industry post type
     */

    $labels = array(
        "name" => __( 'Industries Posts', '' ),
        "singular_name" => __( 'Single Industry Post', '' ),
    );

    $args = array(
        "label" => __( 'Industry', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "industries_posts", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail","excerpt" ),
    );

    register_post_type( "industries_posts", $args );
}

add_action( 'init', 'sms_industries_posts' );

function sms_examples_posts() {

    /**
     * Post Type: Industry post type
     */

    $labels = array(
        "name" => __( 'Examples Posts', '' ),
        "singular_name" => __( 'Single Examples Post', '' ),
    );

    $args = array(
        "label" => __( 'Examples', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "examples_posts", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail","excerpt" ),
    );

    register_post_type( "examples_posts", $args );
}

add_action( 'init', 'sms_examples_posts' );

function sms_examples_taxonomy(){
    register_taxonomy('examples_category', array('examples_posts'), array(
        'label'                 => 'Examples Category',
        'labels'                => array(
            'name'              => 'Examples categories',
            'singular_name'     => 'Examples category',
            'search_items'      => 'Search Category',
            'all_items'         => 'All Category',
            'parent_item'       => 'Parent Category',
            'parent_item_colon' => 'Parent Genre:',
            'edit_item'         => 'Edit Category',
            'update_item'       => 'Update Category',
            'add_new_item'      => 'Add New Category',
            'new_item_name'     => 'New Category Name',
            'menu_name'         => 'Examples category',
        ),
        'description'           => '',
        'public'                => true,
        'publicly_queryable'    => null,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => true,
        'show_in_rest'          => null,
        'rest_base'             => null,
        'hierarchical'          => true,
        'update_count_callback' => '',
        'rewrite'               => array('slug' => "examples_category"),
        //'query_var'             => $taxonomy,
        'capabilities'          => array(),
        'meta_box_cb'           => null,
        'show_admin_column'     => true,
        '_builtin'              => false,
        'show_in_quick_edit'    => null,
    ) );
}


add_action('init', 'sms_examples_taxonomy');

function phone_filter($phone){
    
    if ($phone[0] == '+'){
        $phone_clear = '+'.preg_replace('~\D+~','',$phone);
        return $phone_clear;
    }else{
        $phone_clear = '+1'.preg_replace('~\D+~','',$phone);
        return $phone_clear;
    }
}
