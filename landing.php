<?php
/*
 Template Name: Landing Page
 */
get_header(); ?>

<main id="main">
    <?php
    while ( have_posts() ) : the_post();
        get_template_part( 'template-parts/content', 'page' );
    endwhile;
    ?>
</main>

<div class="prefooter-alternative">
    <div class="container clearfix">
        <?php echo get_custom_logo();?>
        <?php wp_nav_menu(array(
            'theme_location'  => 'header_menu',
            'container'       => 'nav',
            'container_class' => 'main-nav'
        )); ?>
        
    </div>
</div>

<?php
get_footer();
; ?>


