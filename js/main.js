jQuery(document).ready(function ($) {
  $(document).on('click', '.main-header .nav-btn', function () {
    $('.main-header .main-nav-wrapper').addClass('active-mobile');

    $('.active-mobile .menu-item-has-children>a').after().on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).closest('a').next().slideToggle();
    });
  });

  $(document).on('click', '.main-header .close-btn', function () {
    $('.main-header .main-nav-wrapper').removeClass('active-mobile');
  });



  $(document).on('click', '.tabs-feature-container .price-tabs', function () {
    $('.tabs-feature-container .price-tabs').addClass('active');
    $('.tabs-feature-container .price-tabs .active').removeClass('active');
    ;
  });

  $('.main-header').sticky({topSpacing: 1});

  /**
   * header transformation and scroll top btn
   */

  $(document).on('scroll', function () {
    var screenOffsetTop = $(window).scrollTop();

    if (screenOffsetTop <= 100) {
      $('.main-header .header-bottom').removeClass('transformed');
    } else if (screenOffsetTop >= 600) {
      $('.scroll-top-btn').addClass('visible');
    } else {
      $('.main-header .header-bottom').addClass('transformed');
      $('.scroll-top-btn').removeClass('visible');
    }
  });

  /**
   * Sidebar toggleMenu
   */

  (function () {
    var docWidth = $(document).width();
    var menu = $('#sidebar .menu');

    if (docWidth <= 800 && menu.length && menu.find('.current-menu-item').length) {
      createSidebarToggleMenu(menu);
      menu.animate({'opacity': '1'});
    } else if (docWidth <= 800 && menu.length) {
      menu.animate({'opacity': '1'});
    }
  })();

  function createSidebarToggleMenu(menu) {
    var hiddenMenuItems = menu.find('.menu-item');
    var menuItemText = menu.find('.current-menu-item a').text();
    var toggleBtn;

    hiddenMenuItems.hide();
    menu.prepend('<li class="toggle-menu-btn"><a href="#">' + menuItemText + '</a></li>');

    toggleBtn = menu.find('.toggle-menu-btn a');

    toggleBtn.on('click', function (e) {
      e.preventDefault();
      $('.dynamic-anchor-menu').slideToggle();
    });
  }

  /**
   * Sidebar with anchors on Learn Posts Page
   */

  (function () {
    var pageToInit = $('body.learn_posts-template-default');

    if (pageToInit.length) {
      var anchorNames = [];
      var anchorIds = [];

      getAnchorsData();
      createSubMenu();
    }

    function getAnchorsData() {
      var el = $('.block-anchor');

      el.each(function () {
        var name = $(this).find('.title-text-block').text();
        var elId = name.toLowerCase().replace(/ /g, '_');
        $(this).attr('id', elId);

        anchorNames.push(name);
        anchorIds.push(elId);
      });
    }

    function createSubMenu() {
      var toggleBtn = (function () {
        if ($(document).find('.toggle-menu-btn').length) {
          return $('#sidebar .toggle-menu-btn')
        } else {
          return $('#sidebar .current-menu-item');
        }
      })();

      var submenu;

      toggleBtn.append('<ul class="dynamic-anchor-menu"></ul>');
      submenu = $('.dynamic-anchor-menu');

      for (var i = 1; i < anchorIds.length; i++) {
        var menuItem = '<li><a href="' + '#' + anchorIds[i] + '">' + i + '. ' + anchorNames[i] + '</a></li>';
        submenu.append(menuItem);
      }
    }
  })();

  /**
   * Anchors moves
   */
  $('a[href*="#body"]').click(function (event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top - 120
        }, 1000, function () {
          var $target = $(target);
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex', '-1');
          }
        });
      }
    }
  });

  /**
   * Video block Play - Pause
   */

  (function () {
    var myVideo = document.querySelector(".promotion-video");

    $(document).on('click', ".promotion-video", playPause);

    function playPause() {
      if (myVideo.paused)
        myVideo.play();
      else
        myVideo.pause();
    }
  })();

  /**
   * Jumbotrone Awesome
   */

  (function () {
    if ($('.jumbotrone-awesome').length) {
      var container = $('.jumbotrone-awesome');
      var animatedItems = container.find('.single-animated-item');
      var maxSlides = animatedItems.length;
      var count = 0;
      var currentItem = animatedItems[count];

      $(currentItem).fadeIn();

      setInterval(function () {
        changeSlide();
      }, 5000);

      function changeSlide() {
        $(currentItem).fadeOut();

        if (count == (maxSlides - 1)) {
          count = 0;
        } else {
          count++;
        }

        currentItem = animatedItems[count];
        $(currentItem).delay(500).fadeIn(300);
      }
    }
  })();

  /**
   * Animated mobile block
   */
  (function () {
    if ($('.mobile-and-messages-block').length) {
      $('.mobile-and-messages-block').each(function () {
        var animatedBlock = $(this).find('.container');
        var animatedTextBlock = $(this).find('.information').children();
        var animatedElements = $(this).find('.single-message');

        $(animatedBlock).css('opacity', '0');

        $(document).on('scroll', function () {
          var screenOffsetTop = $(window).scrollTop();

          if ($(animatedBlock).offset().top - ($(window).height()) * 0.7 <= screenOffsetTop && !($(animatedBlock).hasClass('animated'))) {
            animateMobileBlockEl(animatedBlock, animatedElements, animatedTextBlock);
          }
        });
      });
    }

    if ($('.mobile-animated-block').length) {
      $('.mobile-animated-block').each(function () {
        var animatedBlock = $(this).find('.container');
        var animatedElements = $(this).find('.single-message');
        $(animatedBlock).css('opacity', '0');

        $(document).on('scroll', function () {
          var screenOffsetTop = $(window).scrollTop();

          if ($(animatedBlock).offset().top - ($(window).height()) * 0.7 <= screenOffsetTop && !($(animatedBlock).hasClass('animated'))) {
            animateMobileBlockEl(animatedBlock, animatedElements);
          }
        });
      });
    }

    function animateMobileBlockEl(wrap, messages, text) {
      $(wrap).addClass('animated');
      TweenLite.to(wrap, 1, {opacity: 1});

      $(text).each(function (index) {
        TweenLite.from($(text)[index], 1, {top: -10, opacity: 0, delay: index / 2});
      });

      messages.each(function (index) {
        TweenLite.from(messages[index], 0.7, {opacity: 0.3, rotationX: "90deg", delay: index / 3});
      });
    }
  })();
});