<?php
/*
 Template Name: Sidebar Left
 */
get_header(); ?>

<div class="two-column-layout">
    <aside id="sidebar">
        <div class="container">
            <?php dynamic_sidebar('sidebar_widget'); ?>
        </div>
    </aside>

    <main id="main">
        <?php
        while (have_posts()) : the_post();
            get_template_part('template-parts/content', 'page');
        endwhile;
        ?>
    </main>
</div>
<?php
get_footer(); ?>


