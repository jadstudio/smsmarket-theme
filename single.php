<?php
/**
 * The template for displaying all single posts
 */
get_header(); ?>
    <?php  get_template_part('template-parts/banners/banner-single-project'); ?>
	<main id="main" class="site-main" role="main">
		<div class="container">
			<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content', get_post_format() );
				endwhile;
			?>
		</div>
	</main><!-- #main -->

<?php get_footer(); ?>