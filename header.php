<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Cache-Control" content="max-age=604800, must-revalidate"/>
  <title><?php bloginfo( 'name' ); ?> | <?php the_title(); ?></title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <script src="https://use.fontawesome.com/1fe1f5d2fb.js"></script>

  <!--GSAP ANIMATION PACKAGE-->
  <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/CSSPlugin.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/easing/EasePack.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>
  <!--END OF GSAP ANIMATION PACKAGE-->

	<?php wp_head(); ?>
</head>

<body id="body" <?php body_class(); ?>>
<header class="main-header">
  <div class="header-top">
    <div class="container">
      <nav>
        <ul class="additional-links">
          <li class="list-item">
            <a href="tel:<?php echo phone_filter( get_option( 'sms_phone_number' ) ); ?>" class="phone-number"><i
                class="fa fa-mobile"></i><?php echo get_option( 'sms_phone_number' ); ?></a>
          </li>

          <li class="list-item">
            <a class="login-link" href="http://mobilesmsmarketing.ca/sms/index.php"><i class="fa fa-user-o"></i>Login</a>
          </li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="header-bottom">
    <div class="container clearfix">
		  <?php echo get_custom_logo(); ?>

      <div class="main-nav-wrapper">
        <div class="flex-wrapper">
				  <?php wp_nav_menu( array(
					  'theme_location'  => 'header_menu',
					  'container'       => 'nav',
					  'container_class' => 'main-nav'
				  ) ); ?>

				  <?php get_template_part( 'template-parts/socials-list' ); ?>
        </div>

        <span class="close-btn">
        <i class="fa fa-close"></i>
      </span>
      </div>

      <span class="nav-btn">
      <i class="fa fa-bars"></i>
    </span>
    </div>
  </div>
</header>

<script>
  jQuery(document).ready(function ($) {
    /*
     *   Mega Menu Init
     */

    (function () {
      var menu = $('.main-nav');

      var thirdLevelMenu = $(menu).find('.menu-item-has-children>.sub-menu .menu-item-has-children .sub-menu');
      var secondLevelMenu = $(thirdLevelMenu).parent().closest('.sub-menu');

      $(thirdLevelMenu).addClass('third-level-menu');
      $(secondLevelMenu).addClass('second-level-menu');

      var groupName = $('.second-level-menu>.menu-item-has-children>a');
      $(groupName).addClass('group-title active');

      $(groupName).on('click', function (e) {
        e.preventDefault();
      });

      $(secondLevelMenu).append('<li class="video-link-wrapper"><a class="video-link" href="http://mobilesmsmarketing.ca/watch-demo/"><i class="fa fa-play-circle-o"></i>Watch Demo</a></li>');
    })();
  });
</script>
