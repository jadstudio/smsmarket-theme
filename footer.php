<div class="pre-footer">
    <div class="container">
        <?php dynamic_sidebar('footer_1'); ?>
        <?php dynamic_sidebar('footer_2'); ?>
        <?php dynamic_sidebar('footer_3'); ?>

        <div class="widget-area widget-area_fourth widget-area_socials">
            <?php get_template_part('template-parts/socials-list') ;?>
        </div>
    </div>
</div>

<footer class="main-footer">
    <div class="container">
        <div class="copy">
            <p><?php echo get_option('sms_copyright'); ?></p>
        </div>
    </div>
</footer>

<a href="#body" class="scroll-top-btn">
    <i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i>
</a>
<?php wp_footer(); ?>

<!--Online chat widget-->
<?php get_template_part('template-parts/widgets/online-chat') ;?>
<!--End of Online chat widget-->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-103313722-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>