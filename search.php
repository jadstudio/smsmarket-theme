<?php
/**
 * The template for displaying search results pages
 */
get_header(); ?>

	<section>
		<div class="container">
			<?php
			if ( have_posts() ) : ?>

				<header class="page-header">
					<h1 class="page-title"><?php printf( __( 'Search Results for: %s', '' ), get_search_query() ); ?></h1>
				</header><!-- .page-header -->

				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content', 'search' );
				endwhile;

				the_posts_navigation();
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif; ?>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>