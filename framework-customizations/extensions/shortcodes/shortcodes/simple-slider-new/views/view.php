<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>

<section class="simple-slider-section hotfix-row">
  <div class="container">
        <div class="slider-wrap">
      <ul class="slides-list">
		    <?php foreach ( $atts['simple_slider'] as $slide ) { ?>
          <li class="single-slide">
            <ul class="new-simple-slider">
            <?php foreach ($slide['simple_slide'] as $slide_img){?>

              <li>
                <img src="<?php echo $slide_img['slide']['url']; ?>" alt="">
              </li>
              <?php }?>
            </ul>

<!--            <div class="image-wrap">-->
<!--              -->
<!--              <img src="--><?php //echo $slide['featured_image']['url']; ?><!--" alt="Alt text">-->
<!--            </div>-->
          </li>
		    <?php } ?>
      </ul>
    </div>
  </div>
</section>

<script>
  jQuery(document).ready(function ($) {
    $('.simple-slider-section .slides-list').bxSlider({
      pager: false,
      nextText: '<i class="fa fa-angle-right"></i>',
      prevText: '<i class="fa fa-angle-left"></i>'
    });
  });
</script>