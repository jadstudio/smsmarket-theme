<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'simple_slider' => array(
		'type' => 'addable-popup',
		'label' => __('Slider', ''),
		'popup-title' => __('Add/Edit Slide', ''),
		'desc' => __('Create your Slide', ''),
		'template' => 'Slide',
		'popup-options' => array(
			'simple_slide' => array(
				'type' => 'addable-popup',
				'label' => __('Slide', ''),
				'popup-title' => __('Add/Edit Slide', ''),
				'desc' => __('Create your Slide', ''),
				'template' => 'Slide',
				'popup-options' => array(
					'slide' => array (
						'type'  => 'upload',
						'images_only' => true,
						'label' => __('Feature Image', ''),
					),
				),
			),
		),
	),
);