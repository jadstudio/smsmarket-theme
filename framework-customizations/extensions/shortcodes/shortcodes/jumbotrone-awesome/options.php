<?php if (!defined('FW')) die('Forbidden');


$options = array(
    'banner_image' => array(
        'desc' => __('Image for banner', ''),
        'type' => 'upload'
    ),
    'title' => array(
        'type' => 'text',
        'label' => __('Title', '')
    ),
    'first_text' => array(
        'type' => 'text',
        'label' => __('Slogan', '')
    ),
    'messages' => array(
        'type' => 'addable-popup',
        'label' => __('Messages', ''),
        'popup-title' => __('Add/Edit Message', ''),
        'desc' => __('Create your button', ''),
        'template' => '{{=bubble_text}}',
        'popup-options' => array(
            'bubble_text' => array(
                'type' => 'text',
                'label' => __('Bubble text', '')
            ),
            'message_text' => array(
                'type' => 'wp-editor',
                'label' => __('Message text', '')
            ),
        ),
    ),
);