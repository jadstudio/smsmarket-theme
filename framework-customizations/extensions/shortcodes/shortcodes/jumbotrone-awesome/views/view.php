<div class="jumbotrone-awesome hotfix-row" style="background-image: url('<?php echo $atts['banner_image']['url']; ?>')">
  <div class="container">
    <div class="info-block">
      <h2 class="title"><?php echo $atts['title'] ?></h2>
      <h4 class="slogan"><?php echo $atts['first_text'] ?></h4>
    </div>

    <div class="mobile-block">
      <ul class="animated-items-list">
				<?php foreach ( $atts['messages'] as $message ) { ?>

          <li class="single-animated-item">
            <div class="message">
							<?php echo $message['message_text']; ?>
            </div>

            <div class="bubble">
              <span><?php echo $message['bubble_text']; ?></span>
            </div>
          </li>

				<?php } ?>

      </ul>
    </div>
  </div>
</div>