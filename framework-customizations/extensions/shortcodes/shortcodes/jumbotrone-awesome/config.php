<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Jumbotrone Awesome', 'smsmarket-theme'),
        'description' => __('Jumbotrone Awesome', 'smsmarket-theme'),
        'tab' => __('Sections', 'smsmarket-theme'),
        'popup_size' => 'medium',
    )
);