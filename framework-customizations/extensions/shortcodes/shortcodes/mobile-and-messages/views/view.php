<div class="mobile-and-messages-block hotfix-row">
    <div class="container">
        <div class="mobile-and-messages">
            <div class="mobile-block">
                <img src="<?php echo $atts['device_image']['url']; ?>" alt="mobile">
            </div>

            <ul class="messages">
                <?php foreach ($atts['messages'] as $message) { ?>
                <li class="single-message">
                    <div class="text">
                        <?php echo $message['message_text']; ?>
                    </div>

                    <div class="bubble">
                        <span><?php echo $message['bubble_text']; ?></span>
                    </div>
                </li>
                <?php } ; ?>
            </ul>
        </div>

        <div class="mobile-placeholder">
            <img src="<?php echo $atts['placeholder_image']['url']; ?>" alt="placeholder device image">
        </div>

        <div class="information">
            <h2><?php echo $atts['title'] ?></h2>
            <?php echo $atts['text_field'] ?>

            <div class="subscribe-form">
                <form action="#">
                    <input type="text" maxlength="60" placeholder="Your Name">
                    <input type="email" maxlength="60" placeholder="Your Email">
                    <input type="submit">
                </form>
            </div>
        </div>
    </div>
</div>