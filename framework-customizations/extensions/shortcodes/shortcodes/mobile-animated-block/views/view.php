<div class="mobile-animated-block hotfix-row" style="background-image: url(<?php echo $atts['bg_image']['url']; ?>)">
    <div class="container">
        <?php if($atts['title']) { ?>
            <h2 class="title"><?php echo $atts['title']; ?></h2>
        <?php } ; ?>

        <div class="mobile-and-messages">
            <div class="mobile-block">
                <img src="<?php echo $atts['device_image']['url']; ?>" alt="mobile">
            </div>

            <ul class="messages">
                <?php foreach ($atts['messages'] as $message) { ?>
                <li class="single-message">
                    <div class="text">
                        <?php echo $message['message_text']; ?>
                    </div>

                    <div class="bubble">
                        <span><?php echo $message['bubble_text']; ?></span>
                    </div>
                </li>
                <?php } ; ?>
            </ul>
        </div>

        <div class="mobile-placeholder">
            <img src="<?php echo $atts['placeholder_image']['url']; ?>" alt="placeholder device image">
        </div>

        <?php if($atts['text_field']) { ?>
            <div class="content-area"><?php echo $atts['text_field']; ?></div>
        <?php } ; ?>
    </div>
</div>