<?php if (!defined('FW')) die('Forbidden');


$options = array(
    'bg_image' => array(
        'desc' => __('Background image', ''),
        'label' => __('Background image', ''),
        'type' => 'upload'
    ),
    
    'title' => array(
        'type' => 'text',
        'label' => __('Title', '')
    ),

    'text_field' => array(
        'type' => 'wp-editor',
        'label' => __('Text', '')
    ),

    'device_image' => array(
        'desc' => __('Device image', ''),
        'type' => 'upload'
    ),

    'placeholder_image' => array(
        'desc' => __('Placeholder image for mobile devices', ''),
        'type' => 'upload'
    ),

    'messages' => array(
        'type' => 'addable-popup',
        'label' => __('Messages', ''),
        'popup-title' => __('Add/Edit Message', ''),
        'desc' => __('Create your message (5 items required)', ''),
        'template' => '{{=bubble_text}}',
        'popup-options' => array(
            'bubble_text' => array(
                'type' => 'text',
                'label' => __('Bubble text', '')
            ),
            'message_text' => array(
                'type' => 'wp-editor',
                'label' => __('Message text', '')
            ),
        ),
    ),
);