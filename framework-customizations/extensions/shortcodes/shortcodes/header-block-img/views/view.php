<div class="block-right-img clearfix">
    <div class="left-content">
        <h2 class="title"><?php echo $atts['header_bck_title']; ?></h2>
        <div class="sub-title">
            <p><?php echo $atts['header_bck_subtitle'];?></p>
        </div>
    </div>
    <div class="right-content">
        <div class="img-wrap">
            <img src="<?php echo $atts['bck_image']['url'];?>" alt="">
        </div>
    </div>
</div>
