<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'header_bck_title' => array(
        'type' => 'text',
        'label' => __('Title', 'fw')
    ),
    'header_bck_subtitle' => array(
        'type' => 'wp-editor',
        'label' => __('Sub Title', 'fw')
    ),
    'bck_image' => array(
        'type' => 'upload',
        'label' => __('Choose Image', ''),
        'desc' => __('Either upload a new, or choose an existing image from your media library', '')
    ),
);