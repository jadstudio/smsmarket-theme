<?php $icon_bgc = (!empty($atts['icon_bgc'])) ? 'background:'.$atts['icon_bgc'].';' : '';?>
<div class="sib-block hotfix-row">
    <div class="container">
        <div class="sib-title">
            <h3><?php echo $atts['sib_title']; ?></h3>
        </div>
        <ul class="sib-item-list">
            <?php 
            $i = 0;

            foreach ($atts['sib_items'] as $item) {
                $image = '';
                if (!empty($item['sib_item_image'])) {
                    $image = $item['sib_item_image']['url'];
                }
                $i++;
            ?>
                <li class="single-sib-item sib-item-<?php echo $i; ?>" style="<?=$icon_bgc;?>">
                    <div class="sib-img <?php if ($item['sib_item_icon']) echo 'fa-icon-in'; ?>">
                        <?php if ($item['sib_item_icon']) { ?>
                            <i class="icon <?php echo $item['sib_item_icon']; ?>"></i>
                        <?php } elseif (!empty($image)) { ?>
                            <img src="<?php echo $image; ?>" alt="icon">
                        <?php }
                        ?>
                    </div>
                    <div class="text-wrapper">
                        <h4 class="sib-item-title"><?php echo $item['sib_item_title']; ?></h4>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
