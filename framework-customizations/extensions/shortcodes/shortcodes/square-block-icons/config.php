<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Square icons block', 'smsmarket-theme'),
        'description' => __('Block with Icons and square background', 'smsmarket-theme'),
        'tab' => __('Sections', 'smsmarket-theme'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);