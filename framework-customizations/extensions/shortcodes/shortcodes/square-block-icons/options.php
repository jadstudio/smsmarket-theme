<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'sib_title' => array(
        'type' => 'text',
        'label' => __('Block Title', 'fw')
    ),

    'icon_bgc' =>array(
        'type'  => 'color-picker',
        'label' => __('Block background color', ''),
    ),
    
    'sib-items' => array(
        'type' => 'addable-popup',
        'label' => __('New item', ''),
        'popup-title' => __('Add/Edit item', ''),
        'desc' => __('Create your item', ''),
        'template' => '{{=sib_item_title}}',
        'popup-options' => array(
            'sib_item_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'sib_item_icon'    => array(
                'type'  => 'icon',
                'label' => __('Choose an Icon', ''),
            ),

            'sib_item_image'   => array(
                'type'  => 'upload',
                'label' => __( 'Choose Image', '' ),
                'desc'  => __( 'Either upload a new, or choose an existing image from your media library', '' )
            ),
        ),
    )
);