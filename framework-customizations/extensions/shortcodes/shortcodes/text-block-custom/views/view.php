<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */
?>
<?php
$class = $atts['class'] ? 'class="block-anchor '.$atts['class'].'"' : 'class="block-anchor" ';
$id = $atts['id'] ? 'id="'.$atts['id'].'"' : '';
$title = $atts['title'] ? '<div class="title-text-block"><h3>'.$atts['title'].'</h3></div>' : '';
echo '<div '.$id.' '.$class.'>';
echo $title;
echo do_shortcode( $atts['text'] );
echo '</div>';
?>