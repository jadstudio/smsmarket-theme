<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title' => array(
		'type' => 'text',
		'label' => __('Title', ''),
	),
	'text' => array(
		'type'   => 'wp-editor',
		'label'  => __( 'Content', 'fw' ),
		'desc'   => __( 'Enter some content for this texblock', 'fw' )
	),
	'attr'             => array(
		'type'    => 'group',
		'options' => array(
			'class'  => array(
				'type'  => 'text',
				'label' => __( 'Class', '' ),
				'desc'  => __( 'Set block Class', '' ),
			),
			'id' => array(
				'type'  => 'text',
				'label' => __( 'ID', '' ),
				'desc'  => __( 'Set block ID', '' ),
			)
		)
	),
);
