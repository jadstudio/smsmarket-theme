<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Text Block Custom', 'fw' ),
	'description' => __( 'Add a Text Block', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);
