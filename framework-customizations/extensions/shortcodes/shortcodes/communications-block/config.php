<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Communications block', ''),
        'description' => __('Communications block', ''),
        'tab' => __('Sections', ''),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);