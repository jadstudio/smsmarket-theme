<div class="communications-block">
    <div class="content">
        <header>
            <h2><?php echo $atts['com_title']; ?></h2>
        </header>
        <ul class="content-ul">
            <?php foreach ($atts['imgs'] as $img) {
                $a = 1; ?>
                <li class="com-content-wrap">
                    <div class="com-img-wrap">
                        <img src="<?php echo $img['img']['url'];?>" alt="">
                    </div>
                    <div class="com-img-desc">
                        <p><?php echo $img['img_desc']; ?></p>
                    </div>
                </li>
            <?php } ?>

        </ul>
        <footer>
            <ul>
                <?php foreach ($atts['links'] as $link) { ?>
                    <li>
                        <a href="<?php echo $link['link_url']; ?>"><?php echo $link['link_title']; ?></a>
                    </li>
                <?php } ?>
            </ul>
        </footer>
    </div>
</div>