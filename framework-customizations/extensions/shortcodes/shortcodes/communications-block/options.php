<?php if (!defined('FW')) die('Forbidden');

$options = array(

    'com_title' => array(
        'type' => 'text',
        'label' => __('Title', '')
    ),

    'imgs' => array(
        'type' => 'addable-popup',
        'label' => __('Images', ''),
        'popup-title' => __('Add/Edit Image', ''),
        'desc' => __('Add Image', ''),
        'template' => '{{=link_title}}',
        'popup-options' => array(
            'img' => array(
                'type' => 'upload',
                'images_only' => true,
                'label' => __('Image', ''),
            ),
            'img_desc' => array(
                'type' => 'text',
                'label' => __('Desc', ''),
            ),
        )
    ),

    'links' => array(
        'type' => 'addable-popup',
        'label' => __('Links', ''),
        'popup-title' => __('Add/Edit Links', ''),
        'desc' => __('Create your Links', ''),
        'template' => '{{=link_title}}',
        'popup-options' => array(
            'link_title' => array(
                'type' => 'text',
                'label' => __('Link title', ''),
            ),
            'link_url' => array(
                'type' => 'text',
                'label' => __('Link address', ''),
            ),
        )
    )
);