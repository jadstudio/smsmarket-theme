<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'resource_title' => array(
        'type' => 'text',
        'label' => __('Title', 'fw')
    ),
    'resources' => array(
        'type' => 'addable-popup',
        'label' => __('New resource', ''),
        'popup-title' => __('Add/Edit resource', ''),
        'desc' => __('Create your resource', ''),
        'template' => '{{=resource_title}}',
        'popup-options' => array(
            'resource_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'resource_icon'    => array(
                'type'  => 'icon',
                'label' => __('Choose an Icon', ''),
            ),

            'image'   => array(
                'type'  => 'upload',
                'label' => __( 'Choose Image', '' ),
                'desc'  => __( 'Either upload a new, or choose an existing image from your media library', '' )
            ),
            'resource_content'   => array(
                'type'  => 'wp-editor',
                'label' => __( 'Content', '' )
            ),
        ),
    )
);