<div class="resources-block hotfix-row">
    <div class="container">
        <div class="resources-title">
            <h3><?php if(!empty($atts['resources_title']))echo $atts['resources_title']; ?></h3>
        </div>
        <ul class="resource-list">
            <?php $i = 0;

            foreach ($atts['resources'] as $key => $resource) {
                $image = '';
                if (!empty($resource['image'])) {
                    $image = $resource['image']['url'];
                }
                $i++; ?>
                <li class="single-resource resource-<?php echo $i; ?>">
                    <div class="resource-img <?php if ($resource['resource_icon']) echo 'fa-icon-in'; ?>">
                        <?php if ($resource['resource_icon']) { ?>
                            <i class="icon <?php echo $resource['resource_icon']; ?>"></i>
                        <?php } elseif (!empty($image)) { ?>
                            <img src="<?php echo $image; ?>" alt="icon">
                        <?php }
                        ?>
                    </div>
                    <div class="text-wrapper">
                        <h4 class="resource-title"><?php echo $resource['resource_title']; ?></h4>
                        <?php echo $resource['resource_content'];
                        ?>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
