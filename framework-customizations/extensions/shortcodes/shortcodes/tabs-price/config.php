<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Price tabs', 'fw' ),
	'description' => __( 'Add some Tabs', 'fw' ),
	'tab'         => __( 'Sections', 'fw' ),
);