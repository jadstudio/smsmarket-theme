<?php if (!defined('FW')) die( 'Forbidden' ); ?>
<?php $tabs_id = uniqid('fw-tabs-');
$a=1;
?>
<?php
/*
 * the `.fw-tabs-container` div also supports
 * a `tabs-justified` class, that strethes the tabs
 * to the width of the whole container
 */
?>
<div class="fw-tabs-container tabs-feature-container" id="<?php echo esc_attr($tabs_id); ?>">
	<div class="fw-tabs tabs-feature">
		<ul>
			<?php foreach ($atts['tabs'] as $key => $tab) : ?>
				<li class ="price-tabs"><a href="#<?php echo esc_attr($tabs_id . '-' . ($key + 1)); ?>"><?php echo $tab['tab_title']; ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
	<?php foreach ( $atts['tabs'] as $key => $tab ) : ?>
		<div class="fw-tab-content price-tab-content" id="<?php echo esc_attr($tabs_id . '-' . ($key + 1)); ?>">
			<ul>
			<?php foreach ($tab['tab_content'] as $ket => $imgs){?>

				<li><div class="img-feature"><img src="<?php echo $imgs['img']['url'];?>" alt=""></div>
				<div class="feature-desc"><p><?php echo $imgs['img_title'];?></p></div>
				</li>
			<?php }?>
			</ul><p><?php //echo do_shortcode( $tab['tab_content'] ) ?></p>
		</div>
	<?php endforeach; ?>
</div>