<?php
$icon_color = (!empty($atts['icon_color'])) ? 'color:'.$atts['icon_color'].';' : '';
$icon_bgc = (!empty($atts['icon_bgc'])) ? 'background:'.$atts['icon_bgc'].';' : '';
?>

<div class="features-mass shortcodes-block hotfix-row">

    <header>
        <h3><?php echo $atts['shortcodes_title']; ?></h3>
    </header>
    <div class="content">
        <ul class="feature-item">
            <?php
            foreach ($atts['shortcodes'] as $key => $shortcode) {
                $a = 1;
                $image = '';
                if (!empty($shortcode['shortcode_image'])) {
                    $image = $shortcode['shortcode_image']['url'];
                } ?>
                <li class="clearfix" style="border-bottom-<?php echo $atts['border'];?>-radius: 0; border-top-<?php echo $atts['border'];?>-radius: 0px">
                    <div class="left">
                        <div class="feature-img <?php if ($shortcode['shortcode_icon']) echo 'fa-icon-in'; ?>" style="<?php if ($shortcode['shortcode_icon']) echo $icon_color.' '.$icon_bgc; ?>">
                            <?php if ($shortcode['shortcode_icon']) { ?>
                                <i class="icon <?php echo $shortcode['shortcode_icon']; ?>"></i>
                            <?php } elseif (!empty($image)) { ?>
                                <img src="<?php echo $image; ?>" alt="icon">
                            <?php }
                            ?>
                        </div>
                    </div>
                    <div class="right">
                        <div class="feature-title">
                            <h3><?php echo $shortcode['shortcode_title']; ?></h3>

                        </div>
                        <div class="feature-content">
                            <p>
                                <?php echo $shortcode['shortcode_content']; ?>

                            </p>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>