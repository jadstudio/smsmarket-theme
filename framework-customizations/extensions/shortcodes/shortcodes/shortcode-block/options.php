<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'shortcodes_title' => array(
        'type' => 'text',
        'label' => __('Title', 'fw')
    ),
    'border' => array(
        'type'  => 'switch',
        'label' => __('Border', ''),
        'left-choice' => array(
            'value' => 'left',
            'label' => __('Left', ''),
        ),
        'right-choice' => array(
            'value' => 'right',
            'label' => __('Right', ''),
        ),
    ),
    'icon_color' =>array(
        'type'  => 'color-picker',
        'label' => __('Icon color', ''),
        ),
    'icon_bgc' =>array(
        'type'  => 'color-picker',
        'label' => __('Icon background color', ''),
    ),
    'shortcodes' => array(
        'type' => 'addable-popup',
        'label' => __('New Fortune', ''),
        'popup-title' => __('Add/Edit Fortune', ''),
        'desc' => __('Create your fortune', ''),
        'template' => '{{=fortune_title}}',
        'popup-options' => array(
            'shortcode_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'shortcode_icon' => array(
                'type' => 'icon',
                'label' => __('Choose an Icon', ''),
            ),
            'shortcode_image' => array(
                'type' => 'upload',
                'label' => __('Choose Image', ''),
                'desc' => __('Either upload a new, or choose an existing image from your media library', '')
            ),
            'shortcode_content' => array(
                'type' => 'textarea',
                'label' => __('Content', 'fw')
            ),
        ),
    ),
);