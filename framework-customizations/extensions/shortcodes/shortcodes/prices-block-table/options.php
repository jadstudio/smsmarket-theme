<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'block_title' => array(
        'type' => 'text',
        'label' => __('Block title', 'fw')
    ),
    'plans' => array(
        'type' => 'addable-popup',
        'label' => __('Plans', ''),
        'popup-title' => __('Add/Edit Price', ''),
        'desc' => __('Create your price', ''),
        'template' => '{{=price_title}}',
        'popup-options' => array(
            'price_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'messages' => array(
                'type' => 'text',
                'label' => __('Messages', 'fw')
            ),
            'keywords' => array(
                'type' => 'text',
                'label' => __('Keywords', 'fw')
            ),
            'users' => array(
                'type' => 'text',
                'label' => __('Users', 'fw')
            ),
            'basic_features' => array(
                'type' => 'text',
                'label' => __('Basic Features', 'fw')
            ),
            'premium_features' => array(
                'type' => 'text',
                'label' => __('Premium Features', 'fw')
            ),
            'price' => array(
                'type' => 'text',
                'label' => __('Price', 'fw')
            ),
            'price_signup_link' => array(
                'type' => 'text',
                'label' => __('Sign up link', '')
            ),
        ),
    ),

    'bottom_text' => array(
        'type' => 'text',
        'label' => __('Bottom text', 'fw')
    ),
);