<?php
$a = 1;

function getPlansInfo($arrayKey, $atts){
    $plansInfo = '';
    $i = 0;
    foreach ($atts['plans'] as $plan){
        if ($plan[$arrayKey] !=''){$i++;}
        $plansInfo .= '<td>'.$plan[$arrayKey].'</td>';
    }

    if ($i == 0) return false;

    return $plansInfo;
}

function getSignUpLink($atts){
    $links = '';
    foreach ($atts['plans'] as $plan){

        $links .= '<td><a href="'.$plan['price_signup_link'].'?plan='.$plan['price_title'].'&price='.$plan['price'].'">Sign up</a></td>';

    }
    return $links;
}
$features = getPlansInfo('premium_features', $atts);

$row_count = ($features) ? 6 : 5;
?>

<div class="prices-block-table hotfix-row">
    <div class="container">
        <h2><?php echo $atts['block_title'];?></h2>
        <table class="price">
            <thead>
            <tr>
                <td>Features \ Plans</td>
                <?php echo getPlansInfo('price_title', $atts);?>
                <td>Enterprise</td>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Messages</td>
                <?php echo getPlansInfo('messages', $atts);?>
                <td rowspan="<?php echo $row_count;?>">Please contact</br>
                    647-646-3350</br>
                    for details</td>
            </tr>
            <tr>
                <td>Keywords</td>
                <?php echo getPlansInfo('keywords', $atts);?>
            </tr>
            <tr>
                <td>Users</td>
                <?php echo getPlansInfo('users', $atts);?>
                </tr>
            <tr>
                <td>Basic Features</td>
                <?php echo getPlansInfo('basic_features', $atts);?>
            </tr>
            <?php if ($features){ ?>
            <tr>
                <td>Premium Features</td>
                <?php echo $features;?>
            </tr>
            <?php }?>
            <tr>
                <td>Price</td>
                <?php echo getPlansInfo('price', $atts);?>
            </tr>
            <tr class="bottom-table">
                <td class="border-none"></td>
                <?php echo getSignUpLink($atts);?>
                <td class="border-none"></td>
            </tr>
            </tbody>
        </table>
        <?php if  ($atts['bottom_text']){
            echo '<p>'.$atts['bottom_text'].'</p>';
        }?>
    </div>
</div>
