<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Prices table block', ''),
        'description' => __('Block for pricing', ''),
        'tab' => __('Sections', ''),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);