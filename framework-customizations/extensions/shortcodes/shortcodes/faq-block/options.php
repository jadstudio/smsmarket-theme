<?php if (!defined('FW')) die('Forbidden');


$options = array(
	'faq_block' => array(
		'type' => 'addable-popup',
		'label' => __('FAQ tab block', ''),
		'popup-title' => __('Add/Edit Block', ''),
		'desc' => __('Create your FAQ Block', ''),
		'template' => '{{=tab_text}}',
		'popup-options' => array(
			'tab_text' => array(
				'type' => 'text',
				'label' => __('Tab title text', '')
			),

			'faq' => array(
				'type' => 'addable-popup',
				'label' => __('Single FAQ Block', ''),
				'popup-title' => __('Add/Edit FAQ', ''),
				'desc' => __('Create your single FAQ', ''),
				'template' => '{{=question}}',
				'popup-options' => array(
					'question' => array(
						'type' => 'text',
						'label' => __('Question', ''),
					),

					'answer' => array(
						'type' => 'wp-editor',
						'label' => __('Answer', ''),
					),
				),
			),
		),
	),
);