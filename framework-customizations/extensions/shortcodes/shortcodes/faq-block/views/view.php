<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>
<div class="faq-block hotfix-row">
  <div class="container">
    <ul class="tab-links-list faq-tabs-list">
			<?php foreach ( $atts['faq_block'] as $block ) {?>
        <li class="list-item">
          <a href="#"><?php echo $block['tab_text'] ?></a>
        </li>
			<?php } ?>
    </ul>

    <ul class="tab-content-list">
			<?php foreach ( $atts['faq_block'] as $block ) { ?>
        <li class="tab-content" >
          <ul class="faq-list">
						<?php foreach ( $block['faq'] as $faq ) { ?>
              <li class="faq-item">
                <h4 class="question">
									<?php echo $faq['question']; ?>
                  <i class="fa fa-plus-square"></i>
                  <i class="fa fa-minus-square"></i>
                </h4>

                <div class="answer">
                  <?php echo $faq['answer']; ?>
                </div>
              </li>
						<?php } ?>
          </ul>
        </li>
			<?php } ?>
    </ul>
  </div>
</div>

<script>
    jQuery(document).ready(function ($) {
        var tabLinks = $('.faq-block .tab-links-list a');
        var tabContent = $('.faq-block .tab-content');
        var questionItem = $('.faq-block .faq-item .question');

        initFaqTabs();

        $(tabLinks).on('click', function(e) {
            var currentTabIndex = $(this).parent().index();
            e.preventDefault();

            if($(tabLinks).hasClass('active')) {
                $(tabLinks).removeClass('active');
            }

            $(this).addClass('active');

            $(tabContent).removeClass('active');
            $(tabContent[currentTabIndex]).addClass('active');
        });

        $(questionItem).on('click', function(e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });

        function initFaqTabs() {
            $(tabLinks[0]).addClass('active');
            $(tabContent[0]).addClass('active');
        }
    })
</script>
