<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('FAQ block', ''),
        'description' => __('Block for FAQ', ''),
        'tab' => __('Sections', ''),
        'popup_size' => 'medium',
    )
);