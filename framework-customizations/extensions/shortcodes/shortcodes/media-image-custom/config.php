<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Image with class', 'fw'),
	'description'   => __('Add an Image', 'fw'),
	'tab'           => __('Media Elements', 'fw'),
);