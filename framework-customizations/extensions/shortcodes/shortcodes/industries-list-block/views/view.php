<div class="industries-list-block hotfix-row">
  <div class="container">
    <ul class="posts-list">
      <?php
        $args = array('post_type' => 'industries_posts', 'posts_per_page' => -1,);
        $loop = new WP_Query($args);
        while ($loop->have_posts()) :
        $loop->the_post(); ?>
        <li class="list-item">
          <div class="wrapper">
            <div class="img-wrap">
              <?php the_post_thumbnail() ; ?>
            </div>

            <div class="text-wrap">
              <h2 class="h3"><?php the_title() ; ?></h2>
              <p><?php the_excerpt() ; ?></p>
            </div>

            <a href="<?php the_permalink() ; ?>" class="btn btn-default">Learn More</a>
          </div>
        </li>
      <?php endwhile;
        wp_reset_postdata()
      ?>
    </ul>
  </div>
</div>