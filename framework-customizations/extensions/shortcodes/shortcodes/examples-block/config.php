<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Examples block', ''),
        'description' => __('Block for examples', ''),
        'tab' => __('Sections', ''),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);