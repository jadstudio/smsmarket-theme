<section class="departments-block hotfix-row">
    <div class="container">
        <h2>Examples of How Departments Use Mobile sms Marketing</h2>
        <ul class="tab-links-list">
            <?php
            $terms_desc = array();
            foreach ($atts['examples'] as $term_id) {
                $args = array(
                    'taxonomy' => 'examples_category',
                    'hide_empty' => false,
                    'term_taxonomy_id' => $term_id['examples_posts'],
                );
                $term = get_terms($args);
                array_push($terms_desc, $term[0]->description);
                ?>
                <li class="list-item">
                    <a href="#"><?php echo $term[0]->name; ?></a>
                </li>

            <?php } ?>
        </ul>

        <ul class="tab-content-list">
            <?php $i = 0;
            foreach ($atts['examples'] as $term_id){
            ?>
            <li class="tab-content-single">
                <header>
                    <p><?php echo $terms_desc[$i];
                        $i++ ?></p>
                </header>
                <?php
                $args = array(
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'examples_category',
                            'field' => 'id',
                            'terms' => $term_id['examples_posts'],
                        )
                    )
                );
                $query = new WP_Query;
                $posts = $query->query($args);
                ?>
                <ul class="examples-list">
                    <?php foreach ($posts as $post) { ?>
                        <li class="example-single">
                            <div class="img-wrap">
                                <img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" alt="alter">
                            </div>

                            <h4><?php echo $post->post_title; ?></h4>
                        </li>
                    <?php } ?>
                </ul>
                <?php } ?>


            </li>
        </ul>
    </div>
</section>

<script>
    jQuery(document).ready(function ($) {
        var tabLinks = $('.departments-block .tab-links-list a');
        var tabContent = $('.departments-block .tab-content-single');

        initFaqTabs();

        $(tabLinks).on('click', function (e) {
            var currentTabIndex = $(this).parent().index();
            e.preventDefault();

            if ($(tabLinks).hasClass('active')) {
                $(tabLinks).removeClass('active');
            }

            $(this).addClass('active');

            $(tabContent).removeClass('active');
            $(tabContent[currentTabIndex]).addClass('active');
        });

        function initFaqTabs() {
            $(tabLinks[0]).addClass('active');
            $(tabContent[0]).addClass('active');
        }
    })
</script>