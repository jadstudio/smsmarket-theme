<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'taxonomy' => 'examples_category',
    'hide_empty' => false,
);
$terms = get_terms( $args );
$terms_array=array();
foreach ($terms as $term) {
    $terms_array[$term->term_id] = $term->name;
}

$options = array(
    'examples_block_title' => array(
        'type' => 'text',
        'label' => __('Title', ''),
    ),
    'examples' => array(
        'type' => 'addable-popup',
        'label' => __('Example', ''),
        'popup-title' => __('Add/Edit Example', ''),
        'desc' => __('Create your Example', ''),
        'template' => '{{=example_title}}',
        'popup-options' => array(
            'examples_descriptions' => array(
                'type' => 'text',
                'label' => __('Descriptions', ''),
            ),
            'examples_posts' => array(
                'type' => 'select',
                'label' => __('Examples', ''),
                'desc' => __('Select price type for show', ''),
                'choices' => $terms_array
            ),
        ),
    ),
);