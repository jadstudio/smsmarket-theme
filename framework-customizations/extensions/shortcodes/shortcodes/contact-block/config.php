<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Contact block', ''),
        'description' => __('Block for Contact', ''),
        'tab' => __('Sections', ''),
        'popup_size' => 'medium',
    )
);