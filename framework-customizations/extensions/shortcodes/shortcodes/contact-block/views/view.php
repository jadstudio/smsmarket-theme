<div class="contact-block">
    <header class="clearfix">
        <div class="contact-title"><h2><?php echo $atts['contacts_title']; ?></h2></div>
        <div class="img-wrap">
            <?php $image = '';
            if (!empty($atts['contacts_image'])) {
                $image = $atts['contacts_image']['url'];
            }

            if ($atts['contacts_icon']) { ?>
                <i class="icon <?php echo $atts['contacts_icon']; ?>"></i>
            <?php } elseif (!empty($image)) { ?>
                <img src="<?php echo $image; ?>" alt="icon">
            <?php }
            ?>
        </div>
    </header>
    <div class="contact-main">
        <ul class="contact-info">
            <?php foreach ($atts['contacts_block'] as $contact) { ?>
                <li>
                    <div class="info-title"><?php echo $contact['contact_title']; ?></div>
                    <div class="info-content"><?php echo $contact['contact_info']; ?></div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>