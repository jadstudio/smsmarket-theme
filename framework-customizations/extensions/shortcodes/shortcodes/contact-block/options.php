<?php if (!defined('FW')) die('Forbidden');


$options = array(
    'contacts_title'=>array(
        'type'=>'text',
        'label'=>__('Title','')
    ),

    'contacts_icon' => array(
        'type' => 'icon',
        'label' => __('Choose an Icon', ''),
    ),

    'contacts_image' => array(
        'type' => 'upload',
        'label' => __('Choose Image', ''),
        'desc' => __('Either upload a new, or choose an existing image from your media library', '')
    ),
    'contacts_block' => array(
        'type' => 'addable-popup',
        'label' => __('Contact block', ''),
        'popup-title' => __('Add/Edit Block', ''),
        'desc' => __('Create your Contact Block', ''),
        'template' => '{{=contact_title}}',
        'popup-options' => array(
            'contact_title' => array(
                'type' => 'text',
                'label' => __('Contact title text', '')
            ),
            'contact_info' => array(
                'type' => 'text',
                'label' => __('Contact info text', '')
            ),
        ),
    ),
);