<div class="jumbotrone-main hotfix-row" style="background-image: url('<?php echo $atts['banner_image']['url'];?>')">
    <div class="container">
        <h2 class="title"><?php echo $atts['title']?></h2>
        <h4 class="slogan"><?php echo $atts['first_text']?></h4>

        <div class="links-wrap">
            <?php foreach ($atts['button'] as $key => $link){
                $i++;
            ?>
            <a href="<?php echo $link['button_link']?>" class="btn btn_large btn-<?php echo $i;?>"><?php echo $link['button_title']?></a>
            <?php } ?>
        </div>

        <div class="afterword">
            <?php echo $atts['second_text'];?>
        </div>
    </div>
</div>