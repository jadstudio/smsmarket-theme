<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Main Jumbotrone', 'smsmarket-theme'),
        'description' => __('Main Jumbotrone', 'smsmarket-theme'),
        'tab' => __('Sections', 'smsmarket-theme'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);