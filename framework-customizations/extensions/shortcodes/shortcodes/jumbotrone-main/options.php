<?php if (!defined('FW')) die('Forbidden');


$options = array(
    'banner_image' => array(
        'desc'  => __( 'Image for banner', '' ),
        'type'  => 'upload'
    ),
    'title' => array(
        'type' => 'text',
        'label' => __('Title', '')
    ),
    'first_text' => array(
        'type' => 'text',
        'label' => __('Text before buttons', '')
    ),
    'button' => array(
        'type' => 'addable-popup',
        'label' => __('Button', ''),
        'popup-title' => __('Add/Edit Button', ''),
        'desc' => __('Create your button', ''),
        'template' => '{{=button_title}}',
        'popup-options' => array(
            'button_title' => array(
                'type' => 'text',
                'label' => __('Button title', '')
            ),
            'button_link' => array(
                'type' => 'text',
                'label' => __('Button link', '')
            ),
        ),
    ),
    'second_text' => array(
        'type' => 'text',
        'label' => __('Text after buttons', '')
    ),
    );