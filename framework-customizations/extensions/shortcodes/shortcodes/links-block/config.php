<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Links block', ''),
        'description' => __('Block with links', ''),
        'tab' => __('Sections', ''),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);