<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'taxonomy' => 'examples_category',
    'hide_empty' => false,
);
$terms = get_terms( $args );
$terms_array=array();
foreach ($terms as $term) {
    $terms_array[$term->term_id] = $term->name;
}

$options = array(
    'links' => array(
        'type' => 'addable-popup',
        'label' => __('Links', ''),
        'popup-title' => __('Add/Edit Links', ''),
        'desc' => __('Create your Links', ''),
        'template' => '{{=link_title}}',
        'popup-options' => array(
            'link_title' => array(
                'type' => 'text',
                'label' => __('Link title', ''),
            ),
            'link_url' => array(
                'type' => 'text',
                'label' => __('Link address', ''),
            ),
        )
    )
);