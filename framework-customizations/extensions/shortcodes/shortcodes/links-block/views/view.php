<section class="departments-block hotfix-row">
    <div class="container">
        <footer>
            <ul>
                <?php foreach ($atts['links'] as $link) { ?>
                    <li>
                        <a href="<?php echo $link['link_url']; ?>"><?php echo $link['link_title']; ?></a>
                    </li>
                <?php } ?>
            </ul>
        </footer>
    </div>
</section>