<?php if (!defined('FW')) {
    die('Forbidden');
} ?>
<section class="blocks-with-images hotfix-row">
    <div class="container">
        <ul class="blocks-list">
            <?php
            $image = '';
            foreach ($atts['block_with_images'] as $block) {

                if (!empty($block['image'])) {
                    $image = $block['image']['url'];
                }
                ?>
                <li class="single-block">
                    <div class="wrap">
                        <div class="image-container <?php if ($block['fa_icon']) echo 'fa-icon-in'; ?>">
                            <?php if ($block['fa_icon']) { ?>
                                <i class="<?php echo $block['fa_icon']; ?>"></i>
                            <?php } else { ?>
                                <img src="<?php echo $image; ?>" alt="icon">
                            <?php }
                            ?>
                        </div>

                        <h3><?php echo $block['title'] ?></h3>

                        <div class="text-wrap">
                            <?php echo $block['content'] ?>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</section>
