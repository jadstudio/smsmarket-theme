<?php if (!defined('FW')) die('Forbidden');


$options = array(
	'block_with_images' => array(
		'type' => 'addable-popup',
		'label' => __('Block with Image', ''),
		'popup-title' => __('Add/Edit Block', ''),
		'desc' => __('Create Block with image', ''),
		'template' => '{{=title}}',
		'popup-options' => array(

			'title'   => array(
				'type'  => 'text',
				'label' => __('Title', '')
			),

			'content' => array(
				'type'  => 'wp-editor',
				'label' => __('Information', '')
			),

			'image'   => array(
				'type'  => 'upload',
				'label' => __( 'Choose Image', '' ),
				'desc'  => __( 'Either upload a new, or choose an existing image from your media library', '' )
			),

			'fa_icon' =>array(
				'type'  => 'icon',
				'label' => __('Icon', ''),
				'desc'  => __('Description', ''),
				'help'  => __('Help tip', ''),
			)
		),
	),
);