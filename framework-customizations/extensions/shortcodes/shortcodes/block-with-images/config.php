<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Section with images', ''),
        'description' => __('Section with images', ''),
        'tab' => __('Sections', ''),
        'popup_size' => 'medium',
    )
);