<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>

<section class="tab-slider-section hotfix-row">
	<div class="container">
		<div class="tab-slider">
      <div id="bx-pager" class="pager">
        <ul class="links-list">
	        <?php
            $slide_count = 0;
            foreach ( $atts['tab_slider'] as $slide ) { ?>
              <li class="list-item">
                <a class="pager-link" data-slide-index="<?php echo $slide_count; $slide_count++; ?>" href="#">
	                <?php echo $slide['title']; ?>
                </a>
              </li>
	        <?php } ?>
        </ul>
      </div>
      
			<ul class="slides-list">
				<?php foreach ( $atts['tab_slider'] as $slide ) { ?>
				<li class="single-slide">
					<div class="video-preview">
            <img src="<?php echo $slide['featured_image']['url']; ?>" alt="Alt text">
					</div>

					<div class="text-block">
            <header>
              <h2><?php echo $slide['title']; ?></h2>
              <?php echo $slide['header_text']; ?>
            </header>

						<ul class="advantages-list">
              <?php foreach ($slide['advantages_list'] as $list) { ?>
							  <li><i class="fa fa-check"></i><?php echo $list['single_advantage']?></li>
							<?php } ?>
						</ul>
					</div>
          
          <div class="links-wrap">
	          <?php foreach ($slide['links'] as $link) { ?>
              <a class="btn btn_special" href="<?php echo $link['link_url']; ?>"><?php echo $link['link_title']; ?></a>
	          <?php } ?>
          </div>
				</li>
        <?php } ?>
			</ul>

      <div class="pagination-wrap">
        <span class="bx-slider-next-btn"></span>
        <span class="bx-slider-prev-btn"></span>
      </div>
		</div>
	</div>
</section>

<script>
jQuery(document).ready(function ($) {
	$('.tab-slider-section .slides-list').bxSlider({
      pagerCustom: '#bx-pager',
      nextSelector: '.tab-slider-section .bx-slider-next-btn',
      prevSelector: '.tab-slider-section .bx-slider-prev-btn',
      nextText: '<i class="fa fa-angle-right"></i>',
      prevText: '<i class="fa fa-angle-left"></i>'
  });
});
</script>