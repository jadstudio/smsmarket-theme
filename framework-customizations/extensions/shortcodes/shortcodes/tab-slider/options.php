<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'tab_slider' => array(
		'type' => 'addable-popup',
		'label' => __('Tab Slide', ''),
		'popup-title' => __('Add/Edit Slide', ''),
		'desc' => __('Create your Slide', ''),
		'template' => '{{=title}}',
		'popup-options' => array(
			'title' => array(
				'type' => 'text',
				'label' => __('Title', '')
			),

			'featured_image' => array (
				'type'  => 'upload',
				'images_only' => true,
				'label' => __('Feature Image', ''),
			),

			'header_text' => array(
				'type' => 'wp-editor',
				'label' => __('Header Text', '')
			),

			'advantages_list' => array(
				'type' => 'addable-popup',
				'label' => __('Advantage', ''),
				'popup-title' => __('Add/Edit Advantage', ''),
				'desc' => __('Create Advantage', ''),
				'template' => 'advantage',
				'popup-options' => array(
					'single_advantage' => array(
						'type' => 'textarea',
						'label' => __('Question', ''),
					),
				),
			),

			'links' => array(
				'type' => 'addable-popup',
				'label' => __('links', ''),
				'popup-title' => __('Add/Edit Link', ''),
				'desc' => __('Create Link', ''),
				'template' => '{{=link_title}}',
				'popup-options' => array(
					'link_title' => array(
						'type' => 'text',
						'label' => __('Link Name', ''),
					),

					'link_url' => array(
						'type' => 'text',
						'label' => __('Link URL', ''),
					),
				),
			),
		),
	),
);