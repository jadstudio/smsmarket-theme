<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title' => __('Tab slider', ''),
		'description' => __('Tab slider block', ''),
		'tab' => __('Sections', ''),
		'popup_size' => 'medium',
		'icon' => 'dashicons dashicons-admin-page',
	)
);