<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Features block', 'smsmarket-theme'),
        'description' => __('Block for features', 'smsmarket-theme'),
        'tab' => __('Sections', 'smsmarket-theme'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);