<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'features' => array(
        'type' => 'addable-popup',
        'label' => __('Features', ''),
        'popup-title' => __('Add/Edit Feature', ''),
        'desc' => __('Create your feature', ''),
        'template' => '{{=feature_title}}',
        'popup-options' => array(
            'feature_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),

            'feature_icon'    => array(
                'type'  => 'icon',
                'label' => __('Choose an Icon', ''),
            ),

            'feature_content' => array(
                'type' => 'wp-editor',
                'value' => '',
                //'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                'label' => __('Content', ''),
                'desc' => __('Use list', ''),
                'help' => __('Use list!', ''),
                'size' => 'small', // small, large
                'editor_height' => 200,
                'wpautop' => true,
                'editor_type' => false, // tinymce, html
            ),
        ),
    )
);