<div class="features-block hotfix-row">
    <div class="container">
        <ul class="features-list">
            <?php
            $i = 0;
            foreach ($atts['features'] as $key => $feature) {
                $i++; ?>
                <li class="single-feature feature-<?php echo $i; ?>">
                    <i class="icon <?php echo esc_attr($feature['feature_icon']); ?>"></i>

                    <div class="text-wrapper">
                        <h4 class="feature-title"><?php echo $feature['feature_title']; ?></h4>
                        <?php echo $feature['feature_content']; ?>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
