<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Posts List', 'smsmarket-theme'),
        'description' => __('List of posts', 'smsmarket-theme'),
        'tab' => __('Sections', 'smsmarket-theme'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);