<div class="new-features-block hotfix-row">
    <div class="container">
        <div class="features-title">
            <h3><?php echo $atts['features_title']; ?></h3>
        </div>
        <ul class="features-list">
            <?php $i = 0;

            foreach ($atts['features'] as $key => $feature) {
                $image = '';
                if (!empty($feature['image'])) {
                    $image = $feature['image']['url'];
                }
                $i++; ?>
                <li class="single-feature feature-<?php echo $i; ?>">
                    <div class="feature-img <?php if ($feature['feature_icon']) echo 'fa-icon-in'; ?>">
                        <?php if ($feature['feature_icon']) { ?>
                            <i class="icon <?php echo $feature['feature_icon']; ?>"></i>
                        <?php } elseif (!empty($image)) { ?>
                            <img src="<?php echo $image; ?>" alt="icon">
                        <?php }
                        ?>
                    </div>
                    <div class="text-wrapper">
                        <h4 class="feature-title"><?php echo $feature['feature_title']; ?></h4>
                        <?php //echo $feature['feature_content'];
                        ?>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
