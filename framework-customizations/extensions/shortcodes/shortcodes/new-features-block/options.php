<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'features_title' => array(
        'type' => 'text',
        'label' => __('Title', 'fw')
    ),
    'features' => array(
        'type' => 'addable-popup',
        'label' => __('New Features', ''),
        'popup-title' => __('Add/Edit Feature', ''),
        'desc' => __('Create your feature', ''),
        'template' => '{{=feature_title}}',
        'popup-options' => array(
            'feature_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'feature_icon'    => array(
                'type'  => 'icon',
                'label' => __('Choose an Icon', ''),
            ),

            'image'   => array(
                'type'  => 'upload',
                'label' => __( 'Choose Image', '' ),
                'desc'  => __( 'Either upload a new, or choose an existing image from your media library', '' )
            ),
        ),
    )
);