<div class="video-block hotfix-row">
    <div class="container">
        <?php echo $atts['pre_video']; ?>
        <div class="video-wrapper">
            <video class="promotion-video" src="<?php echo $atts['video']['url']; ?>" autoplay></video>
        </div>
        <div class="content-wrapper">
            <?php echo $atts['after_video']; ?>
            <a href="<?php echo $atts['link']; ?>" class="btn btn_large"><?php echo $atts['link_title']; ?></a>
        </div>
    </div>
</div>

