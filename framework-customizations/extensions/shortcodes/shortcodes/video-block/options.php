<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$options = array(
    'pre_video' => array(
        'title' => __('Prevideo',''),
        'type' => 'wp-editor',
    ),
    'video' => array(
        'type'  => 'upload',
        'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
        'label' => __('Video link', ''),
        'images_only' => false,
    ),
    'after_video' => array(
        'title' => __('After video',''),
        'type' => 'wp-editor',
    ),
    'link_title' => array(
        'title' => __('Link title',''),
        'type' => 'text',
    ),
    'link' => array(
        'title' => __('Link',''),
        'type' => 'text',
    ),
);