<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title' => __('Industries Icons List', 'smsmarket-theme'),
		'description' => __('List of Industries Icons', 'smsmarket-theme'),
		'tab' => __('Sections', 'smsmarket-theme'),
		'popup_size' => 'medium',
		'icon' => 'dashicons dashicons-admin-page',
	)
);