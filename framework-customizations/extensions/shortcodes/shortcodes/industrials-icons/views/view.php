<section class="industrial-icons hotfix-row">
  <div class="container">
    <h2>Examples of Industries Using Mobile sms Marketing</h2>
    <ul class="icons-list">
			<?php
			$args = array( 'post_type' => 'industries_posts', 'posts_per_page' => 15, );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) :
				$loop->the_post();
                $option_value = fw_get_db_post_option($loop->post->ID, 'fa_icon');?>
        <li class="list-item">
          <a href="<?php the_permalink(); ?>" class="icon-wrap">
            <i class="<?php echo $option_value;?>"></i>
          </a>

          <h3><?php the_title(); ?></h3>
        </li>
			<?php
				wp_reset_postdata();
        endwhile;
			?>
    </ul>
    <a class="btn btn_special btn_centered" href="http://mobilesmsmarketing.ca/industries/">See more industries</a>
  </div>
</section>