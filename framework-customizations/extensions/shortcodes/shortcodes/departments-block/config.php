<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title' => __('Departments Block', ''),
		'description' => __('Examples of How Departments Use Mobile sms1 Marketing', ''),
		'tab' => __('Sections', ''),
		'popup_size' => 'medium',
	)
);