<section class="departments-block hotfix-row">
  <div class="container">
    <h2>Examples of How Departments Use Mobile sms Marketing</h2>
    <ul class="tab-links-list">
      <li class="list-item">
        <a href="#">Human Resources</a>
      </li>

      <li class="list-item">
        <a href="#">Custom Service</a>
      </li>

      <li class="list-item">
        <a href="#">Marketing</a>
      </li>

      <li class="list-item">
        <a href="#">Sales</a>
      </li>

      <li class="list-item">
        <a href="#">IT</a>
      </li>
    </ul>

    <ul class="tab-content-list">
      <li class="tab-content-single">
        <header>
          <p>Are you having trouble getting job candidates or having important email announcements like benefits alerts
            read on time? Then SMS texting will dramatically improve your results.</p>
        </header>

        <ul class="examples-list">
          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>
        </ul>

        <footer>
          <p>For more HR department use cases, click <a href="#">here.</a></p>

          <ul class="links-list">
            <li>
              <a class="btn btn_specific" href="#">Contact us</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Book live demo</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Watch demo</a>
            </li>
          </ul>
        </footer>
      </li>

      <li class="tab-content-single">
        <header>
          <p>Are you having trouble getting job candidates or having important email announcements like benefits alerts
            read on time? Then SMS texting will dramatically improve your results.</p>
        </header>

        <ul class="examples-list">
          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>
        </ul>

        <footer>
          <p>For more HR department use cases, click <a href="#">here.</a></p>

          <ul class="links-list">
            <li>
              <a class="btn btn_specific" href="#">Contact us</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Book live demo</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Watch demo</a>
            </li>
          </ul>
        </footer>
      </li>

      <li class="tab-content-single">
        <header>
          <p>Are you having trouble getting job candidates or having important email announcements like benefits alerts
            read on time? Then SMS texting will dramatically improve your results.</p>
        </header>

        <ul class="examples-list">
          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>
        </ul>

        <footer>
          <p>For more HR department use cases, click <a href="#">here.</a></p>

          <ul class="links-list">
            <li>
              <a class="btn btn_specific" href="#">Contact us</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Book live demo</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Watch demo</a>
            </li>
          </ul>
        </footer>
      </li>

      <li class="tab-content-single">
        <header>
          <p>Are you having trouble getting job candidates or having important email announcements like benefits alerts
            read on time? Then SMS texting will dramatically improve your results.</p>
        </header>

        <ul class="examples-list">
          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>
        </ul>

        <footer>
          <p>For more HR department use cases, click <a href="#">here.</a></p>

          <ul class="links-list">
            <li>
              <a class="btn btn_specific" href="#">Contact us</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Book live demo</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Watch demo</a>
            </li>
          </ul>
        </footer>
      </li>

      <li class="tab-content-single">
        <header>
          <p>Are you having trouble getting job candidates or having important email announcements like benefits alerts
            read on time? Then SMS texting will dramatically improve your results.</p>
        </header>

        <ul class="examples-list">
          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>

          <li class="example-single">
            <div class="img-wrap">
              <img src="http://mobilesmsmarketing.ca/wp-content/uploads/2017/07/healthcare-200x200.jpg" alt="alter">
            </div>

            <h4>Text Communications</h4>
          </li>
        </ul>

        <footer>
          <p>For more HR department use cases, click <a href="#">here.</a></p>

          <ul class="links-list">
            <li>
              <a class="btn btn_specific" href="#">Contact us</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Book live demo</a>
            </li>

            <li>
              <a class="btn btn_specific" href="#">Watch demo</a>
            </li>
          </ul>
        </footer>
      </li>
    </ul>
  </div>
</section>

<script>
jQuery(document).ready(function ($) {
  var tabLinks = $('.departments-block .tab-links-list a');
  var tabContent = $('.departments-block .tab-content-single');

  initFaqTabs();

  $(tabLinks).on('click', function (e) {
    var currentTabIndex = $(this).parent().index();
    e.preventDefault();

    if ($(tabLinks).hasClass('active')) {
      $(tabLinks).removeClass('active');
    }

    $(this).addClass('active');

    $(tabContent).removeClass('active');
    $(tabContent[currentTabIndex]).addClass('active');
  });

  function initFaqTabs() {
    $(tabLinks[0]).addClass('active');
    $(tabContent[0]).addClass('active');
  }
})
</script>