<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'prices' => array(
        'type' => 'addable-popup',
        'label' => __('Price', ''),
        'popup-title' => __('Add/Edit Price', ''),
        'desc' => __('Create your price', ''),
        'template' => '{{=price_title}}',
        'popup-options' => array(
            'price_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'price' => array(
                'type' => 'text',
                'label' => __('Price', 'fw')
            ),

            'price_content_before' => array(
                'type' => 'wp-editor',
                'value' => '',
                //'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                'label' => __('Content before', ''),
                'desc' => __('Content before Sign up button', ''),
                'help' => __('Content before Sign up button', ''),
                'size' => 'small', // small, large
                'editor_height' => 200,
                'wpautop' => true,
                'editor_type' => false, // tinymce, html
            ),
            'price_signup_link' => array(
                'type' => 'text',
                'label' => __('Sign up link', '')
            ),
        ),
    )
);