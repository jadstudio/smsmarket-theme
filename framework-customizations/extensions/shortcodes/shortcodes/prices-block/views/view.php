<div class="prices-block hotfix-row">
    <div class="container">
        <ul class="price-list">
            <?php foreach ($atts['prices'] as $key => $price){
                $i++; ?>
                <li class="single-price price-<?php echo $i;?>">
                    <div class="wrapper">
                        <header>
                            <span class="price-type"><?php echo $price['price_title'];?></span>
                        </header>


                        <div class="offers-list">
                            <div class="price-value">
                                <span class="currency">$</span>
                                <span class="value"><?php echo $price['price'];?></span> /
                                <span class="period">month<span>
                            </div>
                            
                            <div class="info">
                                <?php echo $price['price_content_before'];?>
                            </div>
                            
                            <footer>
                                <a class="btn btn_large sign-up-btn" href="<?php echo $price['price_signup_link'];?>">Sign Up</a>
                            </footer>
                        </div>
                    </div>
                </li>
            <?php }?>
        </ul>
    </div>
</div>
