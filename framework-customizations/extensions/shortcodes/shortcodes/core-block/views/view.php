<section class="departments-block learning-block hotfix-row">
    <div class="container">
        <?php if (!empty($atts['learn_block_title'])) echo '<h2>' . $atts['learn_block_title'] . '</h2>' ?>
        <ul class="tab-links-list">
            <?php foreach ($atts['learn_block_tabs'] as $learn_block_tabs) { ?>
                <li class="list-item">
                    <a href="#"><?php echo $learn_block_tabs['learn_tab_title']; ?></a>
                </li>

            <?php } ?>
        </ul>

        <ul class="tab-content-list">
            <?php $i = 0;
            foreach ($atts['learn_block_tabs'] as $learn_block_tabs) {
                $i++;
                $icon_color = (!empty($learn_block_tabs['icon_color'])) ? 'color:' . $learn_block_tabs['icon_color'] . ';' : '';
                $icon_bgc = (!empty($learn_block_tabs['icon_bgc'])) ? 'background:' . $learn_block_tabs['icon_bgc'] . ';' : '';
                ?>
                <li class="tab-content-single">
                    <ul class="examples-list">
                        <?php foreach ($learn_block_tabs['lear_info'] as $learn_info) {
                            $image = '';
                            if (!empty($learn_info['learn_image'])) {
                                $image = $learn_info['learn_image']['url'];
                            } ?>
                            <li class="example-single">
                                <div class="feature-img <?php if ($learn_info['learn_icon']) echo 'fa-icon-in'; ?>"
                                     style="<?php if ($learn_info['learn_icon']) echo $icon_color . ' ' . $icon_bgc; ?>">
                                    <?php if ($learn_info['learn_icon']) { ?>
                                        <i class="icon <?php echo $learn_info['learn_icon']; ?>"></i>
                                    <?php } elseif (!empty($image)) { ?>
                                        <img src="<?php echo $image; ?>" alt="icon">
                                    <?php }
                                    ?>
                                </div>
                                <h4><?php echo $learn_info['learn_title']; ?></h4>
                                <div class="single-content">
                                    <?php echo $learn_info['learn_desc']; ?>
                                </div>
                                <div class="links-block">
                                    <?php
                                    $video_link = (!empty($learn_info['learn_video'])) ? '<a href="'.$learn_info['learn_video'].'" class="video-link">Video</a>' : '';
                                    $down_link = (!empty($learn_info['learn_down']['url'])) ? '<a href="'.$learn_info['learn_down']['url'].'" class="download-link">Download</a>' : '';
                                    echo $video_link;
                                    echo $down_link;
                                    ?>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
</section>

<script>
    jQuery(document).ready(function ($) {
        var tabLinks = $('.departments-block .tab-links-list a');
        var tabContent = $('.departments-block .tab-content-single');

        initFaqTabs();

        $(tabLinks).on('click', function (e) {
            var currentTabIndex = $(this).parent().index();
            e.preventDefault();

            if ($(tabLinks).hasClass('active')) {
                $(tabLinks).removeClass('active');
            }

            $(this).addClass('active');

            $(tabContent).removeClass('active');
            $(tabContent[currentTabIndex]).addClass('active');
        });

        function initFaqTabs() {
            $(tabLinks[0]).addClass('active');
            $(tabContent[0]).addClass('active');
        }
    })
</script>