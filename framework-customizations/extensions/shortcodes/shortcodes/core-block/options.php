<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'learn_block_title' => array(
        'type' => 'text',
        'label' => __('Title', 'fw')
    ),
    'tab_bgc' =>array(
        'type'  => 'color-picker',
        'label' => __('Tab background color', ''),
    ),
    'learn_block_tabs' => array(
        'type' => 'addable-popup',
        'label' => __('New learn tab', ''),
        'popup-title' => __('Add/Edit tab', ''),
        'desc' => __('Create your tab', ''),
        'template' => '{{=learn_tab_title}}',
        'popup-options' => array(
            'learn_tab_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'icon_color' =>array(
                'type'  => 'color-picker',
                'label' => __('Icon color', ''),
            ),
            'icon_bgc' =>array(
                'type'  => 'color-picker',
                'label' => __('Icon background color', ''),
            ),
            'lear_info' => array(
                'type' => 'addable-popup',
                'label' => __('New learn', ''),
                'popup-title' => __('Add/Edit learn', ''),
                'desc' => __('Create your learn', ''),
                'template' => '{{=learn_title}}',
                'popup-options' => array(
                    'learn_title' => array(
                        'type' => 'text',
                        'label' => __('Title', 'fw')
                    ),
                    'learn_desc' => array(
                        'type' => 'wp-editor',
                        'label' => __('Description', 'fw')
                    ),
                    'learn_video' => array(
                        'type' => 'text',
                        'label' => __('Video link', ''),
                    ),
                    'learn_down' => array(
                        'type'  => 'upload',
                        'value' => array(),
                        'label' => __('Download', ''),
                        'images_only' => false,
                        'files_ext' => array( 'doc', 'pdf', 'zip' ),

                    ),
                    'learn_icon' => array(
                        'type' => 'icon',
                        'label' => __('Choose an Icon', ''),
                    ),

                    'learn_image' => array(
                        'type' => 'upload',
                        'label' => __('Choose Image', ''),
                        'desc' => __('Either upload a new, or choose an existing image from your media library', '')
                    ),
                ),
            ),
        ),
    ),
);