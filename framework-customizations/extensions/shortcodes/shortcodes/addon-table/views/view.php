<div class="prices-block-table hotfix-row" style="background: #fff;">
    <div class="container">
        <h2><?php echo $atts['block_title'];?></h2>
        <table class="price">
            <thead>
            <tr>
                <td>Features</td>
                <td>Price</td>
                <td>Enterprise</td>

            </tr>
            </thead>
            <tbody>

            <?php $i = 0;
            foreach ($atts['addons'] as $addon) { ?>
                <tr>
                    <td>
                        <?php echo $addon['feature']; ?>
                    </td>
                    <td>
                        <?php echo $addon['price']; ?>
                    </td>
                    <?php if ($i == 0) { ?>
                        <td rowspan="6">Please contact</br>
                            647-646-3350</br>
                            for details
                        </td>
                    <?php } ?>
                </tr>
            <?php $i++; } ?>
            </tbody>
        </table>
    </div>
</div>
