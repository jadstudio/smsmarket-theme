<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Addons table', ''),
        'description' => __('Block for addons', ''),
        'tab' => __('Sections', ''),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);