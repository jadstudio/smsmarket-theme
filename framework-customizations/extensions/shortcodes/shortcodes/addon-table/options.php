<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'block_title' => array(
        'type' => 'text',
        'label' => __('Block title', 'fw')
    ),

    'addons' => array(
        'type' => 'addable-popup',
        'label' => __('Plans', ''),
        'popup-title' => __('Add/Edit Price', ''),
        'desc' => __('Create your price', ''),
        'template' => '{{=feature}}',
        'popup-options' => array(
            'feature' => array(
                'type' => 'text',
                'label' => __('Feature', 'fw')
            ),
            'price' => array(
                'type' => 'text',
                'label' => __('Price', 'fw')
            ),
        ),
    )
);