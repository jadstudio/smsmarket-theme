<?php if (!defined('FW')) die('Forbidden');

$args = array(
    'public' => true,
    '_builtin' => false
);

$options = array(
    'features_links_title' => array(
        'type' => 'text',
        'label' => __('Title', 'fw')
    ),
    'features_with_links' => array(
        'type' => 'addable-popup',
        'label' => __('New Fortune', ''),
        'popup-title' => __('Add/Edit Fortune', ''),
        'desc' => __('Create your fortune', ''),
        'template' => '{{=feature_title}}',
        'popup-options' => array(
            'feature_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'feature_sub_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'feature_icon' => array(
                'type' => 'icon',
                'label' => __('Choose an Icon', ''),
            ),

            'feature_image' => array(
                'type' => 'upload',
                'label' => __('Choose Image', ''),
                'desc' => __('Either upload a new, or choose an existing image from your media library', '')
            ),
            'features_list_item' => array(
                'type' => 'addable-popup',
                'label' => __('List', ''),
                'popup-title' => __('Add/Edit List', ''),
                'desc' => __('Create your fortune', ''),
                'template' => '{{=feature_list_item}}',
                'popup-options' => array(
                    'feature_list_item' => array(
                        'type' => 'text',
                        'label' => __('List item', 'fw')
                    ),
                ),
            ),
        ),
    ),
);