<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Block with links', 'smsmarket-theme'),
        'description' => __('Block for for features with links and imgs', 'smsmarket-theme'),
        'tab' => __('Sections', 'smsmarket-theme'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);