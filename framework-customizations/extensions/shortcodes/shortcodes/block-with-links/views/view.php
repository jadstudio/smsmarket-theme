<div class="features-mass hotfix-row">
    <header>
        <h3><?php echo $atts['features_links_title']; ?></h3>
    </header>
    <div class="content">
        <ul class="feature-item">
            <?php
            foreach ($atts['features_with_links'] as $key => $feature) {
                $image = '';
                if (!empty($feature['feature_image'])) {
                    $image = $feature['feature_image']['url'];
                } ?>
                <li class="clearfix">
                    <div class="left">
                        <div class="feature-img">
                            <img src="<?php echo $image; ?>" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="feature-title">
                            <h3><?php echo $feature['feature_title']; ?></h3>
                            <h4><?php echo $feature['feature_sub_title']; ?></h4>
                        </div>
                        <div class="feature-content">
                            <p>
                            <ul class="bullets">
                                <?php foreach ($feature['features_list_item'] as $list_item){?>
                                    <li><?php echo $list_item['feature_list_item']; ?></li>
                                <?php }?>
                            </ul>
                            </p>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>