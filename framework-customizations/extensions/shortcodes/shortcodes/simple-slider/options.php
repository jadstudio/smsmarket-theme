<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'slider_title' => array(
		'type' => 'text',
		'label' => __('Slider Title', '')
	),

	'slider_subtitle' => array(
		'type' => 'wp-editor',
		'label' => __('Slider Subtitle', '')
	),

	'simple_slider' => array(
		'type' => 'addable-popup',
		'label' => __('Slide', ''),
		'popup-title' => __('Add/Edit Slide', ''),
		'desc' => __('Create your Slide', ''),
		'template' => 'Slide',
		'popup-options' => array(
			'featured_image' => array (
				'type'  => 'upload',
				'images_only' => true,
				'label' => __('Feature Image', ''),
			),

			'slide_content' => array(
				'type' => 'wp-editor',
				'label' => __('Slide Text', '')
			),
		),
	),
);