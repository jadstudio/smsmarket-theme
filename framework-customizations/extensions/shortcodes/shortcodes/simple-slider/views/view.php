<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>

<section class="simple-slider-section hotfix-row">
  <div class="container">
    <header>
      <h2 class="title"><?php echo $atts['slider_title'] ;?></h2>
      <h3 class="subtitle"><?php echo $atts['slider_subtitle'] ;?></h3>
    </header>

    <div class="slider-wrap">
      <ul class="slides-list">
		    <?php foreach ( $atts['simple_slider'] as $slide ) { ?>
          <li class="single-slide">
            <div class="image-wrap">
              <img src="<?php echo $slide['featured_image']['url']; ?>" alt="Alt text">
            </div>

            <div class="text-block">
					    <?php echo $slide['slide_content']; ?>
            </div>
          </li>
		    <?php } ?>
      </ul>
    </div>
  </div>
</section>

<script>
  jQuery(document).ready(function ($) {
    $('.simple-slider-section .slides-list').bxSlider({
      pager: false,
      nextText: '<i class="fa fa-angle-right"></i>',
      prevText: '<i class="fa fa-angle-left"></i>'
    });
  });
</script>