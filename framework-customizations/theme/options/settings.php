<?php if (!defined('FW')) die('Forbidden');
$args=array(
    'public'   => true,
    '_builtin' => false
);
$output = 'objects'; // names or objects, note names is the default
$operator = 'and'; // 'and' or 'or'
$post_types=get_post_types( $args, $output, $operator );
$choice_args = array(
  'post' => 'Posts'
);
foreach ($post_types  as $post_type ) {
    $choice_args[$post_type->name] = $post_type->label;
}

$options = array(
    'box_get_started' => array(
        'type' => 'box',
        'options' => array(
            'bgs_title' => array('type' => 'text', 'label' => __('Title', '')),
            'bgs_subtitle' => array('type' => 'text', 'label' => __('Subtitle', '')),
            'bgs_button_1_t' => array('type' => 'text', 'label' => __('First button text', '')),
            'bgs_button_1_l' => array('type' => 'text', 'label' => __('First button link', '')),
        ),
        'title' => __('Get started prefooter block', ''),
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
    ),

    'box_brands' => array(
        'type' => 'box',
        'options' => array(
            'bb_title' => array('type' => 'text', 'label' => __('Title', '')),
            'bb_brands' => array(
                'type' => 'addable-popup',
                'label' => __('Brands', ''),
                'popup-title' => __('Add/Edit Brand', ''),
                'desc' => __('Create your brand', ''),
                'template' => '{{=brand_title}}',
                'popup-options' => array(
                    'brand_title' => array(
                        'type' => 'text',
                        'label' => __('Brand title', ''),
                    ),
                    'brand_image' => array(
                        'desc'  => __( 'Image for brand', '' ),
                        'type'  => 'upload'
                    ),
                ),
            ),
        ),
        'title' => __('Brands prefooter block', ''),
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
    ),

    'banner_block' => array(
        'type' => 'box',
        'options' => array(
            'banner_pages' => array(
                'type'  => 'checkboxes',
//                'value' => array(
//                    'choice-1' => false,
//                    'choice-2' => true,
//                ),
                'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
                'label' => __('Which Pages', ''),
                'desc'  => __('Top banner', ''),
                
                'choices' => $choice_args,
                // Display choices inline instead of list
                'inline' => false,
            ),
            ),
        'title' => __('', ''),
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
    ),
);