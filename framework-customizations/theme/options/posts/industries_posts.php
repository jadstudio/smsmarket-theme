<?php if (!defined('FW')) {
	die( 'Forbidden' );
}

$options = array(
	'ind_icon' =>array(
		'type'    => 'box',
		'title'   => 'Industries Options',
		'options' => array(
			'fa_icon' =>array(
				'type'  => 'icon',
				'label' => __('Label', ''),
				'desc'  => __('Description', ''),
				'help'  => __('Help tip', ''),
			)
		)
	)
);
