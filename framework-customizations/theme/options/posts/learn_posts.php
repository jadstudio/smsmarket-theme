<?php if (!defined('FW')) die('Forbidden');
$page_baner = fw_get_db_settings_option('banner_pages');
if ($page_baner['learn_posts'] == true){
    
$options = array(
    'box_brands_post' => array(
        'type' => 'box',
        'options' => array(
            'banner_post_title' => array('type' => 'text', 'label' => __('Title', '')),
            'banner_post_img' => array(
                'type' => 'upload',
                'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                'label' => __('Image', ''),
                'desc' => __('Image for banner', ''),
                'images_only' => true,
            ),
            'banner_post_link' => array('type'=>'text', 'label' => __('Link','')),
            'banner_post_link_title' => array('type'=>'text', 'label' => __('Link title','')),
        ),
        'title' => __('Banner block', ''),
        'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
    ),
);
}