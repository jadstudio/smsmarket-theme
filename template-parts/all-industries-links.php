<div class="all-industries-links">
	<div class="container">
		<ul class="links-list">
			<?php
			$args = array('post_type' => 'industries_posts', 'posts_per_page' => 6,);
			$loop = new WP_Query($args);
			while ($loop->have_posts()) :
				$loop->the_post();
				$option_value = fw_get_db_post_option($loop->post->ID, 'fa_icon');?>
				<li class="list-item">
					<a class="permalink" href="<?php the_permalink() ; ?>">
            <div class="icon-wrap">
              <i class="<?php echo $option_value;?>"></i>
            </div>

						<h3><?php the_title(); ?></h3>
					</a>
				</li>
			<?php endwhile;
			wp_reset_postdata()
			?>
		</ul>

    <!--Temporary Link-->
    <a href="http://mobilesmsmarketing.ca/industries/" class="btn btn_specific">See More Industries</a>
	</div>
</div>