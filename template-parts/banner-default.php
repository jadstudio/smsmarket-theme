<?php
$banner_post_options = fw_get_db_post_option($post->ID);
//    $banner_post_options_img = fw_get_db_learn_posts_option($post->ID, 'banner_post_img');
//    $banner_post_options_link = fw_get_db_learn_posts_option($post->ID, 'banner_post_link');
//    $banner_post_options_link_title = fw_get_db_learn_posts_option($post->ID, 'banner_post_link_title');
?>
<div class="banner-default" style="background-image: url('<?php echo $banner_post_options['banner_post_img']['url'];?>');">
    <div class="container">
        <div class="text-wrap">
            <h2 class="title"><?php echo $banner_post_options['banner_post_title'];?></h2>
        </div>

        <div class="links-wrap">
            <a href="<?php echo $banner_post_options['banner_post_link'];?>" class="btn btn_large"><?php echo $banner_post_options['banner_post_link_title'];?></a>
        </div>
    </div>
</div>