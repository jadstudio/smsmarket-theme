<div class="get-started-block">
    <div class="container">
        <div class="text-wrap">
            <h2 class="title"><?php echo fw_get_db_settings_option('bgs_title');?></h2>
            <p class="sub-title"><?php echo fw_get_db_settings_option('bgs_subtitle');?></p>
        </div>

        <div class="btn-wrap">
            <a href="<?php echo fw_get_db_settings_option('bgs_button_1_l');?>" class="btn btn_large"><?php echo fw_get_db_settings_option('bgs_button_1_t');?></a><br>
        </div>
    </div>
</div>