<?php
$facebook = get_option('sms_facebook');
$instagram = get_option('sms_instagram');
$twitter = get_option('sms_twitter');
$linkedin = get_option('sms_linkedin');
?>
<ul class="social-links">
    <?php if ($twitter){?>
        <li class="twitter">
            <a href="<?php echo $twitter ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        </li>
    <?php }
    if ($instagram){
        ?>
        <li class="instagram">
            <a href="<?php echo $instagram; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        </li>
    <?php }
    if ($facebook){
        ?>
        <li class="facebook">
            <a href="<?php echo $facebook; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        </li>
    <?php }
    if ($linkedin){
        ?>
        <li class="linkedin">
            <a href="<?php echo $linkedin; ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
        </li>
    <?php } ?>
</ul>