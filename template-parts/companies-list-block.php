<div class="companies-list-block">
    <div class="container">
        <p class="title"><?php echo fw_get_db_settings_option('bb_title');?></p>

        <ul class="companies-list">
            <?php $brands = fw_get_db_settings_option('bb_brands');
            $a=1;
            foreach ($brands as $key => $brand){
                $i++;?>
                <li class="single-company brand-<?php echo $i;?>">
                <img src="<?php echo $brand['brand_image']['url'];?>" alt="<?php echo $brand['brand_title'];?>">
            </li>
            <?php }
            ?>
        </ul>
    </div>
</div>