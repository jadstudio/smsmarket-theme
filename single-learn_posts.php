<?php
get_header(); ?>

<?php
$page_baner = fw_get_db_settings_option('banner_pages');
if ($page_baner[$post_type] == true){
get_template_part('template-parts/banner-default'); }?>

<div class="two-column-layout">
    <aside id="sidebar">
        <div class="container">
            <?php dynamic_sidebar('learn_sidebar_widget'); ?>
        </div>
    </aside>

    <main id="main">
        <div class="container">
            <?php
            while (have_posts()) : the_post();
                get_template_part('template-parts/content', 'page');
            endwhile;
            ?>
        </div>
    </main>
</div>

<?php get_template_part('template-parts/get-started-block') ?>
<?php get_template_part('template-parts/companies-list-block') ?>

<?php
get_footer(); ?>


