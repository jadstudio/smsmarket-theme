<?php get_header(); ?>
  <main id="main" class="site-main" role="main">
    <?php
    while ( have_posts() ) : the_post();
      get_template_part( 'template-parts/content', 'industries' );
    endwhile;
    ?>
  </main><!-- #main -->

  <!--All-industries-list-->
  <?php get_template_part('./template-parts/all-industries-links'); ?>
  <!--End All-industries-list-->
<?php get_footer(); ?>